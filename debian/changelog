open-font-design-toolkit (1.9) unstable; urgency=high

  * Team upload.
  * Drop gozer from Depends since it is unmaintained.

 -- Bastian Germann <bage@debian.org>  Sun, 05 Feb 2023 00:52:06 +0100

open-font-design-toolkit (1.8) unstable; urgency=high

  * Team upload.
  * debian/control:
    + Bump Standards-Version to 4.4.1.
    + Bump debhelper compat to v12.
    - Drop dependency on fontypython. (Closes: #946769)
    - Replace dependency on python-fontforge with recommendation of
      python3-fontforge. (Closes: #945386)
  * debian/compat: Removed.
  * debian/README.Debian: Use secure URIs.
  * debian/copyright: Use Debian Wiki homepage for source field.

 -- Boyuan Yang <byang@debian.org>  Wed, 08 Jan 2020 12:05:30 -0500

open-font-design-toolkit (1.7) unstable; urgency=medium

  * Team upload.
  * Move VCS to salsa.debian.org
  * Drop gnome-specimen from Depends since it is being removed from Debian.
  * Only recommend installing one VCS (git or svn or bzr). Drop other
    recommends.
  * Bump Standards-Version to 4.1.3
  * Bump debhelper compat to 11
  * Use 1.0 format for debian/copyright

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 05 Jan 2018 11:48:57 -0500

open-font-design-toolkit (1.6+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Merge changes from 1.5.1 upload, which got dropped accidentally in the 1.6
    upload (Closes: #857396)

 -- Ivo De Decker <ivodd@debian.org>  Sun, 19 Mar 2017 12:45:53 +0100

open-font-design-toolkit (1.6) unstable; urgency=medium

  * Team upload.
  * debian/control
    - drop fontforge-doc from Depends since now fontforge itself contains
      documents.

 -- Hideki Yamane <henrich@debian.org>  Tue, 14 Feb 2017 07:41:56 +0900

open-font-design-toolkit (1.5.1) unstable; urgency=medium

  * The aim is to make this package suitable for jessie
  * debian/control
    - Remove dependency on currently unmaintained package cl-zpb-ttf
    - Move RC buggy fontmatrix to Recommends
    - set "Standards-Version: 3.9.6" (no change)

 -- Daniel Glassey <wdg@debian.org>  Wed, 05 Nov 2014 18:55:33 +0000

open-font-design-toolkit (1.5) unstable; urgency=medium

  * Team upload.
  * debian/control
    - set "Standards-Version: 3.9.5"
    - move Vcs-* from subversion to git
    - fix "hardcoded depends on shared libraries" (Closes: #731922)
      + drop unnecessary dependency, libsipro0, libfreetype6, libjpeg8, libgif4,
        libuninameslist0 and libtiff5
      + switch dependency from libotf0 to libotf-bin
      + once drop liblais0
    - update debhelper version to 9
  * debian/compat
    - set 9

 -- Hideki Yamane <henrich@debian.org>  Thu, 16 Jan 2014 21:33:48 +0900

open-font-design-toolkit (1.4) unstable; urgency=low

  * Team upload
  * Drop Recommends on bzr-dbus. Closes: #726605

 -- Christian Perrier <bubulle@debian.org>  Thu, 17 Oct 2013 18:39:23 +0200

open-font-design-toolkit (1.3) unstable; urgency=low

  * Team upload
  * No longer include bzr-gtk in the design toolkit. Closes: #722532
  * Update Standards to 3.9.4 (checked)

 -- Christian Perrier <bubulle@debian.org>  Fri, 13 Sep 2013 22:20:39 +0200

open-font-design-toolkit (1.2) unstable; urgency=low

  * typo in README.debian, Closes: #532931
  * debian/control: Don't use giflib transitional packages anymore, Closes: #540531
  * debian/control: Update Standards to 3.9.3
       change section to metapackages so that individual apps can be uninstalled
       without uninstalling ofdt
  * debian/control: depend on grcompiler

 -- Daniel Glassey <wdg@debian.org>  Mon, 21 May 2012 11:14:58 +0700

open-font-design-toolkit (1.1) unstable; urgency=low

  * Team upload

  [ Nicolas Spalinger ]
  * Switch to debhelper v7
  * Use a minimal debian/rules file
  * Switch to 3.0 (quilt) source format
  * Add ${misc:Depends} to dependencies to properly cope with
    debhelper-triggerred dependencies
  * Update Standards to 3.9.2 (checked)

  [ Christian Perrier ]
  * Depend on libjepg8 instread of libjpeg62. Closes: #648501
  * Make this package a native package. There is no upstream.
  * Improve package description

 -- Christian Perrier <bubulle@sesostris.kheops.frmug.org>  Sat, 12 Nov 2011 19:49:26 +0100

open-font-design-toolkit (1.0-4) unstable; urgency=low

  * Update description (Closes: #532931)

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sun, 28 Jun 2009 22:16:33 +0200

open-font-design-toolkit (1.0-3) unstable; urgency=low

  * Adding fontforge-extras
  * Correcting the dependencies for git (Closes: #526149)
  * Typo fixing (Closes: #513188)

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 23 May 2009 17:21:36 +0200

open-font-design-toolkit (1.0-2) unstable; urgency=low

  * Drop some Depends: for unavailable packages

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sat, 01 Nov 2008 15:11:04 +0100

open-font-design-toolkit (1.0-1) unstable; urgency=low

  * Initial release (Closes: #406876)

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Thu, 07 Aug 2008 21:16:32 +0200
